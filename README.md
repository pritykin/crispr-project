# README #

README details the structure of the GuideScan development repository

# Cloning #

When cloning this directory to your machine and running the GuideScan software, please do so in locations with filepaths that do not contain spaces:

Correct Use: /Users/pereza1/Desktop

Incorrect Use: /Users/pereza1/Google\ Drive/

# Install #

### guidescan-crispr ###

guidescan-crispr contains the package scripts for GuideScan. One directory down contains the installation script for the package 
```
#!python

python setup.py build
python setup.py install

```
This directory also contains a subdirectory entitled trie which contains the C++ code for the trie data structure that GuideScan utilizes. Finally, this directory has a subdirectory entitled guidescan which contains the core scripts upon which the GuideScan package depends.

# Dependencies #

GuideScan has the following dependencies:

### Python Version ###
* python 2.7

### Non-Python Dependencies ###
* samtools version 1.3.1
* coreutils: specifically shuf
* rename
* easy_install 

for OSX users these dependencies can be installed via: 
`brew install samtools`,`brew install rename`

for .deb linux users these dependencies can be installed via:
`sudo apt-get install samtools rename`

for .rpm based linux users these dependencies can be installed via:
`sudo yum install samtools rename`

samtools can also be found here: http://www.htslib.org/download/. Unfortunately, versions of samtools <=1.2 are not compatible with GuideScan software.

### Python Dependencies ###
If you run setup.py these installations will be done for you
* biopython>=1.66
* pysam==0.8.3
* pyfaidx==0.4.7.1
* bx-python==0.7.3

If you desire to compute Rule Set 2 on-target cutting efficiency scores 
install sklearn==0.16.1 (must be installed by user, https://pypi.python.org/pypi/scikit-learn/0.16.1)

## Other Directories in Source ##

### guidescan-website ###

guidescan-website contains the python server, HTML, and CSS scripts along with any img or data files that website depends on.

### testing-data ###

testing-data contains a set of .bam, .bed, and .txt files that are used in the local testing of the GuideScan software package as well as local deployment of the GuideScan website.

# Commits #

Details the history of commits throughout the course of GuideScan development. There are two commit tags v0.0.1 and v0.0.2. The first tag represents the history of code used to generate the hg38, mm10, and dm6 sgRNA databases. The second tag represents the history of code used to compute and insert Cas9 cutting efficiency scores into the sgRNA databases.