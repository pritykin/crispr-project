#!/bin/bash

set -e
timestamp=$(date +%s)
screen -dmS 'DONTKILL_'$timestamp bash -c 'sudo python server.py; exec bash'
