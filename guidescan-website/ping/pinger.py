__author__ = 'Alexendar Perez'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""pings GuideScan site and if it determines it is down this script restarts the site"""

#################
#               #
#   Libraries   #
#               #
#################

import os
import sys
import argparse
import subprocess

from time import gmtime, strftime

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--site_url',help='URL of site to ping',required=True)

    args = parser.parse_args()
    site = args.site_url

    return site

def site_check_and_respawn(site):
    """checks site to ensure it is contactable. If it is not then the script attempts to respawn site

    Inputs:

    """
    cmd1 = 'curl -Is %s | head -1' % (site)
    cmd2 = 'bash respawn.sh'
    check = subprocess.check_output(cmd1, shell=True)
    if check:
        sys.stdout.write('%s was contactable at %s \n' % (site, strftime("%Y-%m-%d %H:%M:%S", gmtime())))
    else:
        sys.stdout.write('%s was NOT contactable at %s \nsite respawning attempted at %s \n' % (
        site, strftime("%Y-%m-%d %H:%M:%S", gmtime()), strftime("%Y-%m-%d %H:%M:%S", gmtime())))
        sys.stdout.write('command issued: %s \n' % cmd2)
        os.system(cmd2)

    sys.stdout.write('site check complete \n')

#####################
#                   #
#   Main Function   #
#                   #
#####################

def main():
    """
    site = 'guidescan.com'
    site = 'guidescan.mskcc.org'
    """

    #user input
    site = arg_parser()

    #ping site
    site_check_and_respawn(site)

if __name__ == '__main__':
    main()

